﻿requirejs.config({
    paths: {
        viewModel: 'app/view-model',
        knockout: 'vender/knockout-3.4.2'
    }
});

requirejs(["knockout", "viewModel"],
    function(ko, Model) {
        ko.applyBindings(new Model());
    });