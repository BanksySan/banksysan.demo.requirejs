﻿namespace BanksySan.Demo.RequireJs
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using Nancy;
    using Nancy.Conventions;
    using Nancy.Hosting.Self;

    class Program
    {
        static void Main()
        {
            const string PORT = "9999";

            var baseUri = new Uri($"http://localhost:{PORT}");

            using (var host = new NancyHost(baseUri))
            {
                host.Start();
                Console.WriteLine($"Listening on {baseUri}");

                Console.Read();
            }
        }
    }

    public class IndexModule : NancyModule
    {
        private static readonly IDictionary<string, string> MIME_TYPE_MAPPING = new Dictionary<string, string>
        {
            {"html", "text/html"},
            {"js", "script/"}
        };
        public IndexModule()
        {
            Get["/"] = _ => Response.AsFile("www/index.html");
        }
    }

    public class ApplicationBootstrapper : DefaultNancyBootstrapper
    {
        protected override void ConfigureConventions(NancyConventions nancyConventions)
        {
            nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("/", "www"));

            base.ConfigureConventions(nancyConventions);
        }
    }
}